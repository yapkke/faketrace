#!/usr/bin/env python
import dpkt
import pcap
import threading
import socket
import select
import signal
import struct
import sys

class traceroute_consumer(threading.Thread):
    def __init__(self):
        super(traceroute_consumer, self).__init__()
        self._stop = threading.Event() 

    def run(self):
        self.socks = self._get_sockets()
        while self._stop.isSet():
            self.consume_udp()
            print self.running

    def _get_sockets(self):
        socks = []
        for i in range(33434, 33535):
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.setblocking(0)
            sock.bind(("",i))
            socks.append(sock)
        return socks

    def consume_udp(self):
        rr, wr, er = select.select(self.socks, [], [], 1)
        for rsock in rr:
            print rsock.recvfrom(65536)

class faketrace:
    def __init__(self):
        signal.signal(signal.SIGINT, self.signal_handler)
        
        self.tc = traceroute_consumer()
        self.tc.start()

        self.pc = pcap.pcap()
        self.pc.setfilter('portrange 33434-33534')

        self.routers = []

        self.icmp_sock = socket.socket(socket.AF_INET,
                                       socket.SOCK_RAW,
                                       socket.IPPROTO_ICMP)

        self.eth_sock = socket.socket(socket.PF_PACKET,
                                      socket.SOCK_RAW)
        self.eth_sock.bind(('eth0', 0x0800))

    def signal_handler(signal, frame):
        self.tc._stop.set()
        sys.exit(0)

    def reply_host(self, pkt):
        print "host"
        reply = dpkt.icmp.ICMP(type=dpkt.icmp.ICMP_UNREACH, 
                               code=dpkt.icmp.ICMP_UNREACH_PORT, 
                               data=dpkt.icmp.ICMP.Unreach(data=pkt['data'].pack()))
        ip_dst = socket.inet_ntoa(pkt['data']['src'])
        self.icmp_sock.sendto(reply.pack(), (ip_dst, 1))

    def reply_router(self, pkt):
        n = pkt['data']['ttl']
        print "router %i" % n

        icmp_reply = dpkt.icmp.ICMP(type=dpkt.icmp.ICMP_TIMEXCEED,
                                    code=dpkt.icmp.ICMP_TIMEXCEED_INTRANS,
                                    data=dpkt.icmp.ICMP.TimeExceed(data=pkt['data'].pack()))
        ip_reply = dpkt.ip.IP(src=socket.inet_aton(self.routers[n-1]), 
                              dst=pkt['data']['src'],
                              p=dpkt.ip.IP_PROTO_ICMP,
                              data=icmp_reply)
        ip_reply.len += len(ip_reply['data'])
        
        reply = dpkt.ethernet.Ethernet(src=pkt['dst'],
                                       dst=pkt['src'],
                                       data = ip_reply)

        ip_dst = socket.inet_ntoa(pkt['data']['src'])
        self.eth_sock.send(reply.pack())

    def run(self):
        for ts, pkt in self.pc:
            pkt = dpkt.ethernet.Ethernet(pkt)
            if (pkt['data']['ttl'] > len(self.routers)):
                self.reply_host(pkt)
            else:
                self.reply_router(pkt)
    
    def load_file(self, filename):
        fileRef = open(filename, "r")
        for l in fileRef:
            self.routers.append(l.strip())
        fileRef.close()
            

ft = faketrace()
if (len(sys.argv) > 1):
    ft.load_file(sys.argv[1])
ft.run()


